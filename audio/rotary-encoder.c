#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <wiringPi.h>
#include <alsa/asoundlib.h>

// See all connections in ./xserver/buttons/pinout.h
#define PIN_CLK 17
#define PIN_DT  27

#include <alsa/asoundlib.h>
#include <alsa/mixer.h>

snd_mixer_t *handle;

void handle_ctrl_c(int s){
    fprintf(stderr, "\nGot signal %d, closing ALSA handle, quiting\n", s);
    snd_mixer_close(handle);
    exit(1);
}

int print_volume(long vol, long volume_max){
    float volumePrint = 100*(float)vol/volume_max;
    return printf("volume now: %.1f %% \n", volumePrint);
}

int main(int argc, char **argv){
    // Handle signals
    struct sigaction sigIntHandler;
    sigIntHandler.sa_handler = handle_ctrl_c;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);
 
    // Initialize wiringPi if arguments are legal
    wiringPiSetupGpio();
    fprintf(stderr,
        "%s: Using GPIO PINS CLK=%d, DT=%d\n",
        argv[0], PIN_CLK, PIN_DT
    );
    pinMode(PIN_CLK, INPUT);
    pinMode(PIN_DT, INPUT);

    // Open ALSA stuff
    snd_mixer_open(&handle, 0);
    const char *card = "default";
    snd_mixer_attach(handle, card);
    snd_mixer_selem_register(handle, NULL, NULL);
    snd_mixer_load(handle);
    snd_mixer_selem_id_t *sid;
    snd_mixer_selem_id_alloca(&sid);
    snd_mixer_selem_id_set_index(sid, 0);
    const char *selem_name = "Master";
    snd_mixer_selem_id_set_name(sid, selem_name);
    snd_mixer_elem_t* elem = snd_mixer_find_selem(handle, sid);
    long volume_min, volume_max;
    snd_mixer_selem_get_playback_volume_range(elem, &volume_min, &volume_max);
    long currentVolume_0, currentVolume_1;
    snd_mixer_selem_get_playback_volume(elem, 0, &currentVolume_0);
    snd_mixer_selem_get_playback_volume(elem, 1, &currentVolume_1);
    // Get mean of both channels' volume
    long currentVolume = (currentVolume_0 + currentVolume_1)/2;
    print_volume(currentVolume, volume_max);

    int clkState, readOnce=0, clkLastState = 1;
    while(1) {
        clkState = digitalRead(PIN_CLK);
        if (clkState != clkLastState) {
            if (readOnce == 0) {
                readOnce = 1;
            } else {
                readOnce = 0;
                snd_mixer_selem_get_playback_volume(elem, 0, &currentVolume_0);
                snd_mixer_selem_get_playback_volume(elem, 1, &currentVolume_1);
                // Get mean of both channels' volume
                currentVolume = (currentVolume_0 + currentVolume_1)/2;
                long change;
                if (digitalRead(PIN_DT) != clkState) {
                    // Means: Clockwise rotation
                    change = +1;
                } else {
                    // Means: Anti-clockwise rotation
                    change = -1;
                }
                long newVolume = currentVolume + change*volume_max/100;
                snd_mixer_selem_set_playback_volume_all(elem, newVolume);
                print_volume(newVolume, volume_max);
            }
        }
        clkLastState = clkState;
    }

    return 0;
}

