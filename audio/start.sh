#!/bin/bash
set -e

# We can't run this command in Dockerfile, the space is intention
mount -o remount, rw /lib/firmware
# rsync is better then cp, since I we can never know whether this is our first
# run inside the docker image, or not.
rsync -r /src/piano-firmware/lib/firmware/allo/ /lib/firmware/allo/
# Default values
amixer -c0 sset "Subwoofer mode" 2.1
amixer -c0 sset "Subwoofer" 145

rotary-encoder &

pulseaudio
