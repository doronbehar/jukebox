{ config, lib, pkgs, ... }:

{
  imports = [
    <nixpkgs/nixos/modules/installer/cd-dvd/sd-image-armv7l-multiplatform.nix>
  ];
  nixpkgs = {
    crossSystem.system = "armv7l-linux";
  };
  # HARDWARE
  hardware.deviceTree = {
    enable = true;
    kernelPackage = pkgs.linux_latest;
    overlays = [
      {
        name = "allo-piano-dac-plus-pcm512x-audio";
        dtboFile = "${pkgs.device-tree_rpi.overlays}/allo-piano-dac-plus-pcm512x-audio.dtbo";
      }
    ];
  };
  hardware.firmware = [
    (pkgs.stdenv.mkDerivation {
      # Won't ever be distributed as it's unfree
      name = "piano-firmware-unstable";
      src = pkgs.fetchFromGitHub {
        owner = "allocom";
        repo = "piano-firmware";
        rev = "8a2d7ff1c6a8001602939b88ea1b60f70fa7538c";
        sha256 = "0000000000000000000000000000000000000000000000000000";
      };
      installPhase = ''
        mkdir $out
        cp -r lib $out
      '';
    })
  ];

  # !!! Adding a swap file is optional, but strongly recommended!
  swapDevices = [ { device = "/swapfile"; size = 4096; } ];

  networking.hostName = "NIXPI";
  services.connman = {
    enable = true;
    extraConfig = ''
      [General]
      SingleConnectedTechnology=true
      PreferredTechnologies=ethernet,wifi
    '';
    networkInterfaceBlacklist = [
      "vmnet" "vboxnet" "virbr" "ifb" "ve" "docker0" "docker1" "docker2"
    ];
    extraFlags = [ "--nodnsproxy" ];
    wifi.backend = "iwd";
    enableVPN = false;
  };
  networking.wireless.iwd = {
    enable = true;
  };
  networking.wireless.enable = false;
  networking.firewall.enable = false;

  # connman takes care of that better
  networking.resolvconf.dnsExtensionMechanism = false;
  systemd.services."resolvconf".enable = false;

  documentation.enable = false;
  documentation.nixos.enable = false;
  # closure minification
  environment.noXlibs = true;
  services.xserver.enable = false;
  services.xserver.desktopManager.xterm.enable = lib.mkForce false;
  # this pulls too much graphical stuff
  services.udisks2.enable = lib.mkForce false;
  # this pulls spidermonkey and firefox
  security.polkit.enable = lib.mkForce false;

  # system packages
  environment.systemPackages = with pkgs; [
    neovim
    tinc_pre
    tmux
    tree
    lf
    pciutils
  ];
  # Set your time zone.
  time.timeZone = "Asia/Jerusalem";

  nixpkgs.config = {
    allowUnfree = true;
  };
  systemd.services."nix-daemon".environment = {
    TMPDIR = "/nix/tmp";
  };

  # users & groups
  users.users.doron = {
    isNormalUser = true;
    shell = pkgs.zsh;
    extraGroups = [
      "wheel"
      "audio"
      "transmission"
    ];
  };

  # programs
  programs.zsh.enable = true;
  security.sudo.extraConfig = ''
    Defaults timestamp_type = global
  '';
  programs.less.enable = true;
  services.syncthing.enable = true;
  services.syncthing.group = "transmission";
  services.mpd.enable = true;
  services.mpd.group = "transmission";
  services.mpd.network.listenAddress = "0.0.0.0";
  services.mpd.extraConfig = ''
    audio_output {
      type "pulse"
      name "Local Music Player Daemon"
      server "127.0.0.1"
    }
  '';

  # internationalisation
  i18n = {
    defaultLocale = "en_US.UTF-8";
    extraLocaleSettings = {
      LC_NUMERIC = "en_IL.UTF-8";
      LC_TIME = "en_ZA.UTF-8";
      LC_COLLATE = "he_IL.UTF-8";
      LC_MONETARY = "en_DK.UTF-8";
      LC_MESSAGES = "en_US.UTF-8";
      LC_PAPER = "he_IL.UTF-8";
      LC_NAME = "he_IL.UTF-8";
      LC_ADDRESS = "he_IL.UTF-8";
      LC_TELEPHONE = "he_IL.UTF-8";
      LC_MEASUREMENT = "he_IL.UTF-8";
      LC_IDENTIFICATION = "he_IL.UTF-8";
    };
  };
  # The font that supports Hebrew 
  console.font = "iso08.16";

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;
  hardware.pulseaudio.package = pkgs.pulseaudioFull;
  hardware.pulseaudio.extraModules = [
    pkgs.pulseaudio-modules-bt
  ];
  hardware.pulseaudio.extraConfig = ''
    .ifexists module-bluetooth-policy.so
    load-module module-bluetooth-policy
    .endif
    .ifexists module-bluetooth-discover.so
    load-module module-bluetooth-discover headset=ofono
    .endif
  '';
  hardware.pulseaudio.tcp.enable = true;
  hardware.pulseaudio.tcp.anonymousClients.allowedIpRanges = [
    "127.0.0.1"
    "192.168.14.0/24"
    "192.168.43.0/24"
  ];
}
