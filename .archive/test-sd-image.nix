{ config, lib, pkgs, ... }:

{
  imports = [
    <nixpkgs/nixos/modules/installer/cd-dvd/sd-image-armv7l-multiplatform.nix>
  ];

  nixpkgs = {
    crossSystem.system = "armv7l-linux";
  };

  # closure minification
  environment.noXlibs = true;
  services.xserver.enable = false;
  services.xserver.desktopManager.xterm.enable = lib.mkForce false;
  
  # this pulls too much graphical stuff
  services.udisks2.enable = lib.mkForce false;
  # this pulls spidermonkey and firefox
  security.polkit.enable = false;

  boot.supportedFilesystems = lib.mkForce [ "vfat" ];
  i18n.supportedLocales = lib.mkForce [ (config.i18n.defaultLocale + "/UTF-8") ];

  documentation.enable = false;
  documentation.nixos.enable = false;
}
