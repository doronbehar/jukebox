{ config, pkgs, ... }:

{
  # boot / hardware
  boot.tmpOnTmpfs = true;
  boot.kernelPackages = pkgs.linuxPackages_latest;
  # for the rtl sdr reciever
  services.udev.packages = [ pkgs.rtl-sdr ];
  boot.blacklistedKernelModules = [ "dvb_usb_rtl28xxu" ];

  # NixOS wants to enable GRUB by default
  boot.loader.grub.enable = false;
  boot.loader.raspberryPi = {
    enable = true;
    firmwareConfig = ''
      dtoverlay=allo-piano-dac-plus-pcm512x-audio,glb_mclk
      gpu_mem=256
    '';
    version = 3;
    uboot.enable = true;
  };
  # Enables the generation of /boot/extlinux/extlinux.conf
  boot.loader.generic-extlinux-compatible.enable = true;
  # !!! Needed for the virtual console to work on the RPi 3, as the default of 16M doesn't seem to be enough.
  # If X.org behaves weirdly (I only saw the cursor) then try increasing this to 256M.
  boot.kernelParams = ["cma=32M"];
  # File systems configuration for using the installer's partition layout
  fileSystems = {
    "/boot" = {
      device = "/dev/disk/by-label/NIXOS_BOOT";
      fsType = "vfat";
    };
    "/" = {
      device = "/dev/disk/by-label/NIXOS_SD";
      fsType = "ext4";
    };
  };
  # !!! Adding a swap file is optional, but strongly recommended!
  swapDevices = [ { device = "/swapfile"; size = 4096; } ];

  # networking
  networking.hostName = "NIXPI";
  networking.connman.enable = true;
  networking.wireless = {
    enable = true;
    networks = {
      # Fake ssid so NixOS creates a wpa_supplicant.conf
      # otherwise the service fails and WiFi is not available.
      # see https://github.com/NixOS/nixpkgs/issues/23196
      S4AKR00UNUN21W1NV2Y5MDDW8 = {};
    };
  };
  networking.connman.extraConfig = ''
    [General]
    SingleConnectedTechnology=true
    PreferredTechnologies=ethernet,wifi
  '';
  networking.connman.networkInterfaceBlacklist = [
    "vmnet" "vboxnet" "virbr" "ifb" "ve" "docker0" "docker1" "docker2"
  ];
  networking.connman.extraFlags = [ "--nodnsproxy" ];
  networking.firewall.enable = false;
  # connman takes care of that better
  networking.resolvconf.dnsExtensionMechanism = false;
  systemd.services."resolvconf".enable = false;
  environment.systemPackages = with pkgs; [
    neovim
    tinc_pre
    tmux
    tree
    lf
    pciutils
  ];
  # Set your time zone.
  time.timeZone = "Asia/Jerusalem";

  # nix
  nix.extraOptions = ''
    keep-derivations = false
  '';
  nixpkgs.config = {
    allowUnfree = true;
  };
  systemd.services."nix-daemon".environment = {
    TMPDIR = "/nix/tmp";
  };

  # users & groups
  users.users.doron = {
    isNormalUser = true;
    shell = pkgs.zsh;
    extraGroups = [
      "wheel"
      "audio"
      "transmission"
    ];
  };
  users.groups = {
    transmission = {};
  };

  # programs
  programs.zsh.enable = true;
  security.sudo.extraConfig = ''
    Defaults timestamp_type = global
  '';
  programs.less.enable = true;
  services.syncthing.enable = true;
  services.syncthing.group = "transmission";
  services.mpd.enable = true;
  services.mpd.group = "transmission";
  services.mpd.network.listenAddress = "0.0.0.0";
  services.mpd.extraConfig = ''
    password "qtZlKTu25akr7Kn1QjwRqhGy@read,add,control,admin"
    default_permissions "read"
    audio_output {
      type "pulse"
      name "Local Music Player Daemon"
      server "127.0.0.1"
    }
  '';

  # internationalisation
  i18n = {
    defaultLocale = "en_US.UTF-8";
    extraLocaleSettings = {
      LC_NUMERIC = "en_IL.UTF-8";
      LC_TIME = "en_ZA.UTF-8";
      LC_COLLATE = "he_IL.UTF-8";
      LC_MONETARY = "en_DK.UTF-8";
      LC_MESSAGES = "en_US.UTF-8";
      LC_PAPER = "he_IL.UTF-8";
      LC_NAME = "he_IL.UTF-8";
      LC_ADDRESS = "he_IL.UTF-8";
      LC_TELEPHONE = "he_IL.UTF-8";
      LC_MEASUREMENT = "he_IL.UTF-8";
      LC_IDENTIFICATION = "he_IL.UTF-8";
    };
    consoleUseXkbConfig = true;
    # The font that supports Hebrew
    consoleFont = "iso08.16";
  };

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;
  hardware.pulseaudio.package = pkgs.pulseaudioFull;
  hardware.pulseaudio.extraModules = [
    pkgs.pulseaudio-modules-bt
  ];
  hardware.pulseaudio.extraConfig = ''
    .ifexists module-bluetooth-policy.so
    load-module module-bluetooth-policy
    .endif
    .ifexists module-bluetooth-discover.so
    load-module module-bluetooth-discover headset=ofono
    .endif
  '';
  hardware.pulseaudio.tcp.enable = true;
  hardware.pulseaudio.tcp.anonymousClients.allowedIpRanges = [
    "127.0.0.1"
    "192.168.14.0/24"
    "192.168.43.0/24"
  ];
}
