(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 13.1' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      8904,        248]
NotebookOptionsPosition[      8209,        227]
NotebookOutlinePosition[      8601,        243]
CellTagsIndexPosition[      8558,        240]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[{
 RowBox[{
  RowBox[{"M", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"a1", ",", " ", "b1", ",", " ", "c1"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"a2", ",", "b2", ",", "c2"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0", ",", "0", ",", "1"}], "}"}]}], "}"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"MatrixForm", "[", "M", "]"}]}], "Input",
 CellChangeTimes->{{3.8905400407287416`*^9, 3.890540112474102*^9}, {
  3.890542439928281*^9, 3.890542443182312*^9}},
 CellLabel->
  "In[248]:=",ExpressionUUID->"6473309e-fa65-4779-a670-2822e009ffa4"],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {"a1", "b1", "c1"},
     {"a2", "b2", "c2"},
     {"0", "0", "1"}
    },
    GridBoxAlignment->{"Columns" -> {{Center}}, "Rows" -> {{Baseline}}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{3.890540114212435*^9, 3.8905424437617607`*^9, 
  3.8905424766507473`*^9},
 CellLabel->
  "Out[249]//MatrixForm=",ExpressionUUID->"9a34fe2f-bd8e-4205-ab25-\
7a59ce2fa118"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", "\[IndentingNewLine]", 
   RowBox[{
   "The", " ", "natural", " ", "Corners", " ", "where", " ", "obtained", " ", 
    "using", " ", "xdotool", " ", "getmouselocation", " ", "on", " ", "ssh", 
    " ", "to", " ", "xserver", " ", "host", "\[IndentingNewLine]", "The", " ",
     "order", " ", "of", " ", "the", " ", "numbers", " ", 
    RowBox[{"is", ":", " ", "\[IndentingNewLine]", " ", 
     RowBox[{"1", " ", "2", "\[IndentingNewLine]", "3", " ", "4"}]}]}], 
   "\[IndentingNewLine]", "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"naturalCorner1", "=", 
     RowBox[{"{", 
      RowBox[{"184", ",", " ", "455", ",", "1"}], "}"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"transformedCorner1", " ", "=", 
     RowBox[{"{", 
      RowBox[{"0", ",", "0", ",", "1"}], "}"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"naturalCorner2", "=", 
     RowBox[{"{", 
      RowBox[{"175", ",", "0", ",", "1"}], "}"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"transformedCorner2", "=", 
     RowBox[{"{", 
      RowBox[{"0", ",", "480", ",", "1"}], "}"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"naturalCorner3", "=", 
     RowBox[{"{", 
      RowBox[{"443", ",", "454", ",", "1"}], "}"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"transformedCorner3", "=", 
     RowBox[{"{", 
      RowBox[{"0", ",", "480", ",", "1"}], "}"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"naturalCorner4", "=", 
     RowBox[{"{", 
      RowBox[{"423", ",", "0", ",", "1"}], "}"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"transformedCorner4", "=", 
     RowBox[{"{", 
      RowBox[{"480", ",", "480", ",", "1"}], "}"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"eq1", "=", 
     RowBox[{
      RowBox[{"M", ".", "naturalCorner1"}], "==", "transformedCorner2"}]}], 
    ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"eq2", "=", 
     RowBox[{
      RowBox[{"M", ".", "naturalCorner2"}], "==", "transformedCorner1"}]}], 
    ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"eq3", "=", 
     RowBox[{
      RowBox[{"M", ".", "naturalCorner3"}], "==", "transformedCorner3"}]}], 
    ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"eq4", "=", 
     RowBox[{
      RowBox[{"M", ".", "naturalCorner4"}], "==", "transformedCorner4"}]}], 
    ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"sol", "=", 
     RowBox[{"Solve", "[", 
      RowBox[{"{", 
       RowBox[{"eq2", ",", "eq2", ",", "eq3", ",", "eq4"}], "}"}], "]"}]}], 
    ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"sol1", "=", 
     RowBox[{"Part", "[", 
      RowBox[{"sol", ",", "1"}], "]"}]}], ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"finalM", "=", 
     RowBox[{"N", "[", 
      RowBox[{"M", "/.", "sol1"}], "]"}]}], ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"finalM", "[", 
      RowBox[{"[", 
       RowBox[{"1", ",", "3"}], "]"}], "]"}], "=", 
     RowBox[{"finalM", "[", 
      RowBox[{"[", 
       RowBox[{"1", ",", "3"}], "]"}], "]"}]}], ";"}], "\[IndentingNewLine]", 
   
   RowBox[{
    RowBox[{
     RowBox[{"finalM", "[", 
      RowBox[{"[", 
       RowBox[{"2", ",", "3"}], "]"}], "]"}], "=", 
     RowBox[{"finalM", "[", 
      RowBox[{"[", 
       RowBox[{"2", ",", "3"}], "]"}], "]"}]}], ";"}], "\[IndentingNewLine]", 
   
   RowBox[{"MatrixForm", "[", "finalM", "]"}]}]}]], "Input",
 CellChangeTimes->{{3.890540308218423*^9, 3.890540324370819*^9}, {
   3.890540575838826*^9, 3.890540595455727*^9}, {3.8905406426411953`*^9, 
   3.890540800201643*^9}, {3.890541000132666*^9, 3.890541078462656*^9}, {
   3.890541153174777*^9, 3.890541154304613*^9}, {3.890541976421625*^9, 
   3.890542030337935*^9}, {3.8905421578331623`*^9, 3.890542161526952*^9}, {
   3.890542212766376*^9, 3.890542215004562*^9}, {3.8905423139303102`*^9, 
   3.890542370306734*^9}, {3.8905424051809998`*^9, 3.890542405576193*^9}, {
   3.890542447904481*^9, 3.890542734521736*^9}, 3.890542782193285*^9, {
   3.890542816689927*^9, 3.890542821515779*^9}, {3.890542922668597*^9, 
   3.890542926017638*^9}, {3.89054316762607*^9, 3.890543181426756*^9}, {
   3.890544335286178*^9, 3.890544336753025*^9}, {3.890544603593767*^9, 
   3.890544608356945*^9}, {3.890544651937701*^9, 3.890544655760212*^9}, {
   3.890544687577545*^9, 3.8905449420334187`*^9}, {3.890544975579973*^9, 
   3.890544990459022*^9}, {3.890545043376995*^9, 3.8905450454592457`*^9}, {
   3.89054546795296*^9, 3.8905454699818068`*^9}, {3.8905455489331636`*^9, 
   3.890545562808894*^9}},
 CellLabel->
  "In[889]:=",ExpressionUUID->"ed998bf9-3ce2-4d1b-b794-9565d3c0cf5a"],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {"1.935483870967742`", 
      RowBox[{"-", "1.1425323291175216`"}], 
      RowBox[{"-", "338.7096774193548`"}]},
     {"1.935483870967742`", 
      RowBox[{"-", "0.08526360665056132`"}], 
      RowBox[{"-", "338.7096774193548`"}]},
     {"0.`", "0.`", "1.`"}
    },
    GridBoxAlignment->{"Columns" -> {{Center}}, "Rows" -> {{Baseline}}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{
  3.8905420357744303`*^9, 3.890542165923321*^9, 3.890542215723651*^9, {
   3.890542365272792*^9, 3.890542371174077*^9}, 3.89054240750345*^9, {
   3.890542452981949*^9, 3.8905425491245737`*^9}, {3.8905425792009*^9, 
   3.8905426017520237`*^9}, {3.890542636879468*^9, 3.8905426730818777`*^9}, {
   3.8905427177054977`*^9, 3.890542737254219*^9}, 3.890542782675252*^9, {
   3.890542817646599*^9, 3.8905428221707573`*^9}, 3.890542926423571*^9, {
   3.890543168670063*^9, 3.890543181796503*^9}, 3.890544342407736*^9, 
   3.89054494424415*^9, {3.890544978093348*^9, 3.8905449908067017`*^9}, 
   3.890545045987783*^9, 3.890545470407156*^9, 3.8905455644182253`*^9, 
   3.8905456017490873`*^9},
 CellLabel->
  "Out[906]//MatrixForm=",ExpressionUUID->"8380cae6-7ec0-401f-844e-\
2fe478bb0ba5"]
}, Open  ]]
},
WindowSize->{720., 734.25},
WindowMargins->{{0, Automatic}, {0, Automatic}},
FrontEndVersion->"13.1 for Linux x86 (64-bit) (June 16, 2022)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"b7020c8e-7703-4e21-af80-af5f42292818"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 600, 16, 54, "Input",ExpressionUUID->"6473309e-fa65-4779-a670-2822e009ffa4"],
Cell[1183, 40, 710, 21, 80, "Output",ExpressionUUID->"9a34fe2f-bd8e-4205-ab25-7a59ce2fa118"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1930, 66, 4745, 122, 531, "Input",ExpressionUUID->"ed998bf9-3ce2-4d1b-b794-9565d3c0cf5a"],
Cell[6678, 190, 1515, 34, 80, "Output",ExpressionUUID->"8380cae6-7ec0-401f-844e-2fe478bb0ba5"]
}, Open  ]]
}
]
*)

