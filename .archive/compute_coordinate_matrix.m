total_area_width = 480;
total_area_height = 800;
touch_area_width = 480;
touch_area_height = 480;

% TODO: considered left (/ right?)
touch_area_x_offset = 0;
% TODO: considered up (/ down?)
touch_area_y_offset = 270;

c0 = touch_area_width / total_area_width;
c2 = touch_area_height / total_area_height;
c1 = touch_area_x_offset / total_area_width;
c3 = touch_area_y_offset / total_area_height;

% Source for reference:
% https://wiki.archlinux.org/title/Calibrating_Touchscreen#Calculate_the_Coordinate_Transformation_Matrix
m_resize = [ c0 0.0 c1;
             0  c2  c3;
             0  0   1 ];

% fdisp(2, "m_resize:");
% fdisp(2, m_resize);

% Given - from around the web
m_rotate = [ 0.0 -1.0 1.0;
             1.0  1.0 0.0;
	         0.0  0.0 1.0 ];
fdisp(2, "m_rotate:");
fdisp(2, m_rotate);

% Move
m_move =[ 1.0 0.0 (800-270)/800;
	      0.0 1.0 ]

fdisp(2, "m_final:");
% m_final = m_resize * m_rotate ;
m_final = m_resize;
m_final = m_rotate;
% m_final = m_rotate * m_resize;
fdisp(2, m_final);

s_final_command = "env DISPLAY=:0 xinput set-prop 6 --type=float 'Coordinate Transformation Matrix'";
for idx = 1:numel(m_final)
	s_final_command = [s_final_command ' ' num2str(m_final(idx), "%f")];
endfor

disp(s_final_command)
