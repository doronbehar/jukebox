#!/usr/bin/env bash

# kill previous x11 sockets that have persisted
rm -r /tmp/.X11-unix 2>/dev/null

/usr/bin/entry.sh echo "Running balena base image entrypoint..."

echo "Setting initial display to FORCE_DISPLAY - $FORCE_DISPLAY"

# destroy any leftover X11 lockfile. credit to @danclimasevschi
# https://github.com/balenablocks/xserver/issues/16
DISP_NUM=$(echo "$FORCE_DISPLAY" | sed "s/://")
LOCK_FILE="/tmp/.X${DISP_NUM}-lock"
if [ -f "$LOCK_FILE" ]; then
    echo "Removing lockfile $LOCK_FILE"
    rm -f "$LOCK_FILE" &> /dev/null
fi

export DBUS_SYSTEM_BUS_ADDRESS=unix:path=/host/run/dbus/system_bus_socket

echo "balenaBlocks xserver version: $(cat VERSION)"

if [ "$CURSOR" = true ];
then
    exec startx -- $FORCE_DISPLAY
else
    exec startx -- $FORCE_DISPLAY -nocursor
fi
