// Used for printf() statements
#include <stdio.h>
#include <string.h>
#include <wiringPi.h>

#include "pinout.h"

// Support printing a counter when the rotary encoder changes
#ifdef DEBUG_COUNTER
int counter = 0;
#endif

void EN_while1(char **argv, int clkPin, int dtPin) {
    printf(
        "%s %s: Using GPIO PINS CLK=%d, DT=%d\n",
        argv[0], argv[1], clkPin, dtPin
    );
#ifdef DEBUG_COUNTER
    printf(
        "%s %s: Printing counter state due to DEBUG_COUNTER defined!\n",
        argv[0], argv[1]
    );
#endif
    pinMode(clkPin, INPUT);
    pinMode(dtPin, INPUT);
    int clkState, readOnce=0, clkLastState = 1;
    while(1) {
        clkState = digitalRead(clkPin);
        if (clkState != clkLastState) {
            if (readOnce == 0) {
                readOnce = 1;
            } else {
                readOnce = 0;
                if (digitalRead(dtPin) != clkState) {
                    // Means: Clockwise rotation
                    fprintf(stderr, ">\n");
#ifdef DEBUG_COUNTER
                    counter++;
#endif
                } else {
                    // Means: Anti-clockwise rotation
                    fprintf(stderr, "<\n");
#ifdef DEBUG_COUNTER
                    counter--;
#endif
                }
#ifdef DEBUG_COUNTER
                fprintf(stderr, "count: %d\n", counter);
#endif
            }
        }
        clkLastState = clkState;
    }
}

// Delays
#define DEBOUNCE_DELAY 100

void SW_while1(char **argv, int swPin){
    printf(
        "%s %s: Using GPIO PIN %d\n",
        argv[0], argv[1], swPin
    );
    pinMode(swPin, INPUT);
    int currentRead = 1;
    int previousRead = 1;
    int wasPressed = 0;
    int pressedNow = 0;
    int wasReleased = 0;
    // the last time the output pin was toggled
    unsigned long pressTimer = 0;
    unsigned long releaseTimer = 0;
    while(1) {
        currentRead = digitalRead(swPin);
        if (currentRead == 0 && previousRead == 1) {
            pressTimer = millis();
            wasPressed = 1;
            //printf("Detected press\n");
        }
        if ((millis() - pressTimer) > DEBOUNCE_DELAY && wasPressed) {
            //printf("Detected real press!\n");
            pressedNow = 1;
        }
        if (currentRead == 1 && previousRead == 0 && pressedNow) {
            //printf("Detected release\n");
            wasPressed = 0;
            pressedNow = 0;
            wasReleased = 1;
        }
        if ((millis() - releaseTimer) > DEBOUNCE_DELAY && wasReleased) {
            //printf("Detected real release!\n");
            wasReleased = 0;
            // Means "pressed", from some reason using printf here doesn't work
            // with awesome's `awful.spawn.with_line_callback`, see:
            // https://www.reddit.com/r/awesomewm/comments/12ce9y6/c_program_using_printf_doesnt_play_with/
            fprintf(stderr, "p\n");
        }
        previousRead = currentRead;
        //delay(1000);
    }
}

int main(int argc, char **argv){
    int arg_SW_r = 0;
    int arg_EN_l = 0;
    int arg_SW_l = 0;
    if (argc == 2) {
        // Middle encoder is not handled here - it's handled in:
        // ../../audio/rotary-encoder-volume-daemon.c
        arg_SW_r = (strcmp(argv[1],"right-switch" ) == 0);
        arg_EN_l = (strcmp(argv[1],"left-encoder" ) == 0);
        arg_SW_l = (strcmp(argv[1],"left-switch"  ) == 0);
    }
    int legal_argument = arg_SW_r + arg_EN_l + arg_SW_l;
    if (argc == 1 || !legal_argument || argc > 2) {
        fprintf(stderr,
            "%s: ERROR: Illegal command line usage. Run `%s <ARG>` with one of the following:\n"
            " - right-switch\n"
            " - left-encoder\n"
            " - left-switch\n",
            argv[0], argv[0]
        );
        return 7;
    }
    // Up until here we only checked the arguments

    // Initialize wiringPi if arguments are legal
    wiringPiSetupGpio();

    if (arg_EN_l) {
        EN_while1(argv, PIN_LEFT_CLK, PIN_LEFT_DT);
    } else if (arg_SW_r) {
        SW_while1(argv, PIN_RIGHT_SW);
    } else if (arg_SW_l) {
        SW_while1(argv, PIN_LEFT_SW);
    }
    return 0;
}
