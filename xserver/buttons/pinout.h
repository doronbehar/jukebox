#define PIN_RIGHT_SW  4
// Can be read during operation, but having these connected at boot ruins the
// DAC's initialization.
//
//#define PIN_RIGHT_CLK 2
//#define PIN_RIGHT_DT  3

//#define RE_MIDDLE_SW // Not connected
#define PIN_MIDDLE_CLK 17
#define PIN_MIDDLE_DT  27

#define PIN_LEFT_SW    23
#define PIN_LEFT_CLK   24
#define PIN_LEFT_DT    22

/**** ----------------- ****
 **** PIANO J19 details ****
 **** ----------------- ****

  COLOR  |  PROG  |   RPI   |  J19 . J19  |   RPI   |   PROG   |  COLOR  
=========|========|=========|======|======|=========|==========|=========
  WHITE  |        |   +5V   |  01  |  02  |   +5V   |          |   RED   
         |        |    NC   |  03  |  04  |    NC   |          |         
         |        | GPIO 02 |  05  |  06  | GPIO 04 |   RSW    |  GREY   
         |        | GPIO 03 |  07  |  08  | GPIO 17 |   MCLK   |  BLACK  
         |        |    NC   |  09  |  10  | GPIO 27 |   MDT    |  WHITE  
         |        |    NC   |  11  |  12  | GPIO 24 |   LCLK   |  GREEN  
  ORANGE |  LSW   | GPIO 23 |  13  |  14  | GPIO 22 |   LDT    |  YELLOW 
  BLACK  |        |   GND   |  15  |  16  |   GND   |          |  BROWN  
*/
