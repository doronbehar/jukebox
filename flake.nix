{
  description = "JukeBox source files";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";

  outputs = { self
  , nixpkgs
  , flake-utils
  }: flake-utils.lib.eachDefaultSystem (system:
  let
    pkgs = nixpkgs.legacyPackages.${system};
  in {
    devShells.default = pkgs.mkShell {
      nativeBuildInputs = [
        pkgs.rpiboot
      ];
      buildInputs = [
        # For developing buttons-daemon
        pkgs.wiringpi
        pkgs.alsa-lib
        (pkgs.lua5_3.withPackages(lp: [
          lp.luasocket
          lp.luacheck
        ]))
      ];
    };
  });
}
