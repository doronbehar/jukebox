I have a [Kali reclocker](https://www.allo.com/sparky/kali-reclocker.html) and [Piano 2.1 DAC](https://www.allo.com/sparky/piano-dac-2-1.html) and I experience a somewhat well known issue where playing certain songs produces a horrible loud white noise. I found a solution for some platforms that use MPD internally:

1. https://audiophilestyle.com/forums/topic/54630-kali-reclocker-and-piano-21-24-bit-white-noise-problem/
2. https://community.volumio.org/t/kali-reclocker-white-noise-with-24-bit-music/8449/3
3. http://moodeaudio.org/forum/showthread.php?tid=40&pid=247#pid247
 
All of the solutions are the same, quoting:

> The workaround is to set MPD SoX resampling to 32 bit / *kHz.

But I don't know how to implement them in pure MPD configuration. I know I can setup a resampler with:

```
resampler {
  plugin "soxr"
  quality "very high"
}
```

But how do I set bit depth of the resampler to 32 bit? I also thought of using a custom recipe If anyone can give other tips how to debug the issue, I'd be delighted.
